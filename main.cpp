#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "videoloader.hpp"
#include "detectorcontroller.hpp"
#include "timer.hpp"

using namespace cv;
using namespace std;

string TIMER_TEST="test";

int main(int argc, char** argv )
{
   Timer t;
   DetectorController det=DetectorController();
   //VideoLoader vid=VideoLoader("ergo1.avi",0,70,true);
   VideoLoader vid=VideoLoader("ergo1.mp4",290,514,true);
   //VideoLoader vid=VideoLoader("ergo5.mp4",0,24*30+1000,true);
   //VideoLoader vid=VideoLoader("ergo5_cropped.mp4",0,24*30+1000,true);
   //VideoLoader vid=VideoLoader("ergo7.mp4",0,10000,true);
   //VideoLoader vid=VideoLoader("ergo8.mp4",0,24*30+1000,true);
   //VideoLoader vid=VideoLoader("ergo9.mp4",0,24*30+10000,true);
   char c=0;
   try {
   while (c!='q') {
      long ticks=getTickCount();
      t.start(TIMER_TEST);
      Mat f=vid.next();//vid.next();
      /*vector<Mat> v;
      split(f,v);
      for (int i=0; i<3; i++)
      equalizeHist(v[i],v[i]);
      merge(v,f);*/
      //normalize(f,f,0,255,NORM_MINMAX);
    //  GaussianBlur(f,f,Size(3,3),0,0);
      //normalize(f,f,0,255,NORM_MINMAX);
//flip(f,f,1);
      //det.loopDetector(vid.next());
det.loopDetector(f);

//det.loopDetector(f);
      c=waitKey(0);
      t.stop("test");
      //printf("%f ms\n",(getTickCount()-ticks)/getTickFrequency()*1000);
   }
   } catch (char const* s) {
     printf(s);
   }
   int t1=t.numBlocks(TIMER_TEST);
   printf("Tot: %f, avg: %f, num: %i\n",t.totalMs(TIMER_TEST),t.avgMs(TIMER_TEST),t1);
   det.getTimer().print();
   return 0;
}

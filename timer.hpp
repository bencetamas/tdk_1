#ifndef __TIMER_HPP__
#define __TIMER_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>
#include <map>

using namespace cv;
using namespace std;

struct TimerEntry {
   int64 sumTicks;
   int64 lastEntry;
   bool active;
   int numBlocks;
   TimerEntry() :
      sumTicks(0),
      lastEntry(0),
      active(false),
      numBlocks(0)
   {
   }
};

class Timer {
   private:
      map<String,TimerEntry> times;
   public:
      void print() {
         for (map<String,TimerEntry>::iterator it=times.begin(); it!=times.end(); it++)
            cout << it->first
                      << "," << it->second.sumTicks/getTickFrequency()/1000.0
                      << "," << it->second.sumTicks/getTickFrequency()/1000.0/it->second.numBlocks
                      << "," << it->second.numBlocks << endl ;
      }

      void start(string algorithm) {
         TimerEntry en;
         map<String,TimerEntry>::iterator it=times.find(algorithm);
         if (it!=times.end()) {
            en=it->second;
         } else
           en.numBlocks=0;
         if (en.active)
            throw "Cannot start a running timer.";
         en.active=true;
         en.lastEntry=getTickCount();
         times[algorithm]=en;
      }

      void stop(string algorithm) {
         map<String,TimerEntry>::iterator it=times.find(algorithm);
         if (it==times.end())
            throw "Timer not found";
         TimerEntry en=it->second;
         if (!en.active)
            throw "Cannot stop a timer, which is idle.";
         en.active=false;
         en.sumTicks=en.sumTicks+(getTickCount()-en.lastEntry);
         en.numBlocks=en.numBlocks+1;
         times[algorithm]=en;
      }

      double totalMs(string algorithm) {
         map<String,TimerEntry>::iterator it=times.find(algorithm);
         if (it==times.end())
            throw "Timer not found";
         TimerEntry en=it->second;
         return en.sumTicks/getTickFrequency()*1000.0;
      }

      double avgMs(string algorithm) {
         map<String,TimerEntry>::iterator it=times.find(algorithm);
         if (it==times.end())
            throw "Timer not found";
         TimerEntry en=it->second;
         if (en.numBlocks>0)
	    return en.sumTicks/getTickFrequency()/en.numBlocks*1000.0;
         else
            return 0.0;
      }

      double numBlocks(string algorithm) {
         map<String,TimerEntry>::iterator it=times.find(algorithm);
         if (it==times.end())
            throw "Timer not found";
         TimerEntry en=it->second;
         return en.numBlocks;
      }
};

#endif



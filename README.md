To run the software, you need the *Make*, *CMake* (at least version 2.8) and *GNU c++ compiler* (at least version 4.4) programs being installed. A properly installed version of OpenCV 3 (we tested with the version 3.1.0) software library is also required. Please note, that installing OpenCV could very challenging on some operating systems. The code should run on Windows, Mac and Linux operating systems also. (The code was tested on Ubuntu 14.04.)

After cloning or downloading the git repository, open a terminal or command prompt, go to the downloading location and type the following commands:

```bash
cmake .
make
```

This should produce an executable in the root project directory called $main$. Type

```bash
./main
```

to run the program. Note that in the current version (as of 21th October, 2016) the name of the processed video file is hard-wired to the code. To open another video file, edit the name in the `main.cpp` source file and recompile the program. The program can support various video file formats, but the list supported video formats depends on the settings of current OpenCV installation.

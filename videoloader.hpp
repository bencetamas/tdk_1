#ifndef __VIDEOLOADER_HPP__
#define __VIDEOLOADER_HPP__

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

using namespace cv;
using namespace std;

class VideoLoader {
   private:
      VideoCapture* cap;
      string fname;
      int from;
      int to;
      int current;
      bool repeat;
      double resizeRatio;

      void loadVideo() {
         if (cap!=NULL)
            cap->release();
         cap=new VideoCapture(fname);
         if (!cap->isOpened())
            throw "Error when opening video file";
         for (int i=0; i < from; i++)
            if (!cap->grab())
               throw "No more video frames!";
         current=from;
         double w=cap->get(CAP_PROP_FRAME_WIDTH);
         double h=cap->get(CAP_PROP_FRAME_HEIGHT);
         resizeRatio=1.0;
         while (w > 320 || h > 240) {
            w=w/2;
            h=h/2;
            resizeRatio=resizeRatio*2.0;
         }
         resizeRatio=1.0/resizeRatio;
      }
   public:
      VideoLoader(string fname, int from=0, int to=-1, bool repeat=true) {
         cap=NULL;
         this->fname=fname;
         this->from=from;
         this->to=to;
         this->repeat=repeat;
         loadVideo();
      }


      ~VideoLoader() {
         if (cap!=NULL) {
            cap->release();
            delete cap;
         }
      }

      Mat next() {
         current=current+1;
         if (to>0 && current>=to) {
            if (repeat) {
               loadVideo();
               Mat frame;
               if (!cap->read(frame))
                  throw "No more video frames!";
	       resize(frame,frame,Size(0,0),resizeRatio,resizeRatio,INTER_LINEAR);
               return frame;
            } else {
                  throw "No more video frames!";
            }   
         } else {
            Mat frame;
            if (!cap->read(frame))
                  if (repeat) {
                     loadVideo();
                     if (!cap->read(frame))
                        throw "No more video frames!";
	             resize(frame,frame,Size(0,0),resizeRatio,resizeRatio,INTER_LINEAR);
                     return frame;
            } 
	    resize(frame,frame,Size(0,0),resizeRatio,resizeRatio,INTER_LINEAR);
            return frame;
         }
      }
};

#endif



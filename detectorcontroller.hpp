#ifndef __DETECTORCONTROLLER_HPP__
#define __DETECTORCONTROLLER_HPP__

/*
TODO:
--Contour detector: csapásjellemzők meghatározása
--fej pozíció követése
--boka pozíció megahtározása
--csípő - térd - váll - kézfej ...
*/

#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <fstream>
#include <math.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <queue>

#include "timer.hpp"

//#define DEBUG

using namespace cv;
using namespace std;

const double videoFps=30.0;
const double minPhaseTimeInSec=0.5;
const double strokePhaseHysteresis=0.3;

const int boundaryDetectionThreshold=40; // threshold for separating background-foreground in RGB space
const int boundaryDetectionKernelSize=3; // morphology kernel size (rectangle)
const int boundaryDetectionStepSize=4; // every "stepSize"th image is processed and diff with each other
const double boundaryDetectionMaxXDelta1=0.07; // at least this should be reached after the catch for diffing with the catch image
const double boundaryDetectionMaxXDelta2=0.4;
const double boundaryDetectionMom00Ratio1=0.3; // at least these ratio (number of points) should be reached to diffing with catch imag
const double boundaryDetectionMom00Ratio2=0.1;
const int boundaryDetectionMaxXFrameBufferSize=10; // store this number of frames after a catch is happened
const bool boundaryDetectionCalcBoundingBox=true;
const bool boundaryDetectionCalcBoundingHull=false;
const int boundaryDetectionWindowSize=250;

const int contourDetectionThreshold=40; // threshold for separating background-foreground in RGB space
const int contourDetectionThreshold2=40; // threshold for separating background-foreground in RGB space
const int contourDetectionKernelSize=1; // morphology kernel size (rectangle)
const int contourDetectionHandKernelSize=2;
const int contourDetectionWindowSize=10250;
const double contourDetectionFlipDecisionRatio=2.0/5.0; // the upper half of the bounding box is tracked
const int contourDetectionLastFlipDecisionTime=130; // after that no decision is made
const bool contourDetectionMakeFlipDecision=true;
const double contourDetectionFlipDecisionRatio2=1.0; // head pos at min/max max ratio
const int contourDetectionHipYDetectorWidth=10;
const double contourDetectionHipYAlpha=0.4;
const bool contourDetectionEnableTracking=true;
const bool contourDetectionFilterContour=true;
const int contourDetectionShoulderPointSearchRectW=26;
const int contourDetectionShoulderPointSearchRectH=4;
const double contourDetectionUpperLowerArmRatio=30.0/33.0;
const int contourDetectionBackYStepSize=2;

const double componentTrackingHeadTorsoRatio=1.0/2.5;
const int componentTrackingErodeKernelSize=1;
const int componentTrackingDilateKernelSize=0;

const bool compareBackgroundSubtractionMethods=false;
const bool compareBackgroundSubtractionMethodsOnlyPhase1=false;

Timer timer;

class BoundaryDetectionStatus {
   double CM_max_x;
   Mat frame_max_x;
   vector<Mat> frames_after_max_x;
   int left_min_x;
   Mat frame_min_x;
   Mat bg;
   bool bgChanged;
   int max_mom00;

   Mat last_frame;
   Mat kernel;
   int t;
   Mat cumulativeMask;
   int cumulativeMaskMax;
   bool convexHullMaskChanged;
   bool hullChanged;
   Rect boundingBox;
   Rect dilBoundingBox;
   vector<Point> hullCache;
   Mat convexHullMaskCache;

   void updateCumulativeMask(Mat mask) {
      if (!boundaryDetectionCalcBoundingHull)
         return;

      convexHullMaskChanged=true;
      hullChanged=true;
      if (cumulativeMask.empty()) {
         cumulativeMask=mask;
         cumulativeMaskMax=1;
      } else {
         cumulativeMask=cumulativeMask+mask;
         cumulativeMaskMax=cumulativeMaskMax+1;
         if (cumulativeMaskMax==255) {
            threshold(cumulativeMask,cumulativeMask,1,1,THRESH_BINARY);
            cumulativeMaskMax=1;
         }
      }
   }

   Rect updateBoundingBox(Mat mask) {
      if (!boundaryDetectionCalcBoundingBox)
         return Rect(-1,-1,-1,-1);
      Rect curr=boundingRect(mask);
      if (boundingBox.x==-1)
         boundingBox=curr;
      else {
         Rect tmp;
         tmp.x=max(0,min(curr.x,boundingBox.x));
         tmp.y=max(0,min(curr.y,boundingBox.y));
         tmp.width=min(mask.cols,max(boundingBox.x+boundingBox.width,curr.x+curr.width)); // here width is the x coord of the br corner
         tmp.width=tmp.width-tmp.x;
         tmp.height=min(mask.cols,max(boundingBox.y+boundingBox.height,curr.y+curr.height)); // here height is the y coord of the br corner
         tmp.height=tmp.height-tmp.y;
         boundingBox=tmp;
      }
      return curr;
   }

   Mat calcMask(Mat f1,Mat f2) {
      Mat mask;
      absdiff(f1,f2,mask);
      vector<Mat> channels;
      split(mask,channels);
      mask=channels[0];
      for (int i=1; i < channels.size(); i++) {
         mask=mask+channels[i];
      }
      threshold(mask,mask,boundaryDetectionThreshold,1,THRESH_BINARY);
      if (boundaryDetectionKernelSize>0)
         erode(mask,mask,kernel);
      return mask;
   }

   Mat getBoundingMask() {
      if (cumulativeMask.empty())
         throw "No cumulative mask calculated yet!";
      if (cumulativeMaskMax>1) {
         threshold(cumulativeMask,cumulativeMask,1,1,THRESH_BINARY);
         cumulativeMaskMax=1;
      }
      return cumulativeMask;
   }

   Mat getDilatedBoundingMask() {
      Mat cumM=getBoundingMask().clone();
      if (boundaryDetectionKernelSize>0)
         dilate(cumM,cumM,kernel);
      return cumM;
   }

public:
   void registerNewObservation(Mat frame) {
      if (frames_after_max_x.size()<boundaryDetectionMaxXFrameBufferSize)
        frames_after_max_x.push_back(frame);

      t=t+1;
      if (t%boundaryDetectionStepSize!=0)
         return;

      if (!last_frame.empty()) {
         Mat mask=calcMask(last_frame,frame);
         Moments mom=moments(mask);
         if (mom.m00>max_mom00)
            max_mom00=mom.m00;
         if (mom.m00>0) {
            updateCumulativeMask(mask);
            Rect br=updateBoundingBox(mask);
            double CM_curr_x=mom.m10/mom.m00;
            if (CM_curr_x > CM_max_x) {
               CM_max_x=CM_curr_x;
               frame_max_x=frame;
               bgChanged=true;
               frames_after_max_x.clear();
            }
            double left_curr_x= br.x==-1 ? boundingRect(mask).x : br.x;
            if (left_curr_x < left_min_x) {
               left_min_x=left_curr_x;
               frame_min_x=frame;
	           bgChanged=true;
            }
         }

         #ifdef DEBUG
         imshow("testBoundaryDetectionStatusMask",mask);
         imshow("testBoundaryDetectionStatus",frame);
         printf("testBoundaryDetectionStatus: minx: %f, maxx: %f, curr_x:%f\n",CM_min_x, CM_max_x, last_CM_x);
         #endif
      }
      last_frame=frame;
   }

   vector<Point> getConvexHull() {
      if (!boundaryDetectionCalcBoundingHull)
         return vector<Point>();

      if (!hullChanged)
         return hullCache;

      Mat mask=getDilatedBoundingMask();
      vector<vector<Point> > contours;
      vector<Vec4i> hierarchy;
      findContours( mask, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
      for (int i=1; i<contours.size(); i++)
        for (int j=0; j<contours[i].size(); j++)
           contours[0].push_back(contours[i][j]);

      vector<vector<Point> > hull(1);
      convexHull(Mat(contours[0]),hull[0]);
      hullCache=hull[0];
      hullChanged=false;
      return hull[0];
   }

   Mat getConvexHullMask() {
      if (!boundaryDetectionCalcBoundingHull)
         return Mat();

      if (!convexHullMaskChanged)
         return convexHullMaskCache;

      vector<Point> hull=getConvexHull();
      vector<vector<Point> >hull_;
      hull_.push_back(hull);
      Mat mask=Mat::zeros(Size(last_frame.cols,last_frame.rows),CV_8UC1);
      drawContours(mask,hull_,0,Scalar(255),FILLED);
      convexHullMaskCache=mask;
      convexHullMaskChanged=false;
      return mask;
   }

   Rect getBoundingBox() {
      if (!boundaryDetectionCalcBoundingBox)
         return Rect(-1,-1,-1,-1);

      return boundingBox;
   }

   Rect getDilatedBoundingBox() {
      if (!boundaryDetectionCalcBoundingBox)
         return Rect(-1,-1,-1,-1);

      Rect tmp;
      tmp.x=max(0,boundingBox.x-boundaryDetectionKernelSize);
      tmp.y=max(0,boundingBox.y-boundaryDetectionKernelSize);
      tmp.width=min(last_frame.cols-tmp.x,boundingBox.width+2*boundaryDetectionKernelSize);
      tmp.height=min(last_frame.rows-tmp.y,boundingBox.height+2*boundaryDetectionKernelSize);
      return tmp;
   }

   bool isReady() {
      return t > boundaryDetectionWindowSize && frames_after_max_x.size()>=boundaryDetectionMaxXFrameBufferSize;
   }

   Mat getBg() {
      if (bgChanged) {
         bg=frame_max_x.clone();

         int left_max_x=0;
         double delta=CM_max_x-left_min_x;
         int i=0;
         double curr_x;
         Moments mom;
         do {
             Mat mask=calcMask(frame_max_x,frames_after_max_x[i]);
             mom=moments(mask);
             if (mom.m00>0) {
                curr_x=mom.m10/mom.m00;
                left_max_x=boundingRect(mask).x;
             } else
                curr_x=mask.cols*100;
             i++;
         } while (i<frames_after_max_x.size() && ((curr_x > CM_max_x-boundaryDetectionMaxXDelta1*delta
                                                   || mom.m00<max_mom00*boundaryDetectionMom00Ratio1)
                                                 && (curr_x > CM_max_x-boundaryDetectionMaxXDelta2*delta
                                                   || mom.m00<max_mom00*boundaryDetectionMom00Ratio2)));


         Rect frame_min_x_rect=Rect(left_max_x,0,bg.cols-left_max_x,bg.rows);
         Mat tmp=bg(frame_min_x_rect);
         frame_min_x(frame_min_x_rect).copyTo(tmp);

         bgChanged=false;
      }
      return bg.clone();
   }

   BoundaryDetectionStatus() :
      CM_max_x(-1),
      left_min_x(10000),
      t(0),
      bgChanged(true),
      max_mom00(0),
      cumulativeMaskMax(0),
      boundingBox(-1,-1,-1,-1),
      convexHullMaskChanged(true),
      hullChanged(true)
   {
      if (boundaryDetectionKernelSize>0)
         kernel = getStructuringElement( MORPH_RECT,
                                       Size( 2*boundaryDetectionKernelSize + 1, 2*boundaryDetectionKernelSize + 1 ),
                                       Point( boundaryDetectionKernelSize, boundaryDetectionKernelSize ) );
   }
};

class ContourDetectionStatus {
   Mat bg;

   int right_max_x;
   int left_max_x;
   Mat frame_max_x;
   Mat mask_max_x;
   double CM_min_x;
   Mat frame_min_x;
   double CM_last_x;
   double filtered_CM_last_x;
   double direction_last; // = curr_x-last_x
   double direction_avg;
   double head_CM_max_x_estimate;
   double head_CM_min_x_estimate;

   Mat computedBg;
   bool bgChanged;
   int t;
   Mat kernel;
   Mat handKernel;

   Rect bbox;
   Mat bhull;
   Size origSize;
   int t_catch;
   int t_finish;
   int d_catch_catch;
   int d_finish_finish;
   bool should_make_flip_decision;
   double hipy;
   int hip_topy;
   int hipx;
   int hip_x_from_back;
   double thight_top;
   Point handPoint;
   Point anklePoint;
   double thightLength;
   Point kneePoint;
   Point lastHeadPosition;
   Point shoulderPoint;
   double shoulderLength;
   Point elbowPoint;
   vector<Point> backCurve;
   bool backIsHidedByElbow;

   int catchBackPos;
   int catchHipPos;
   int catchHandPos;
   int lastBackContribution;
   int lastLegContribution;
   int lastArmContribution;

   Mat calcMask(Mat f1,Mat bg, int thresh=contourDetectionThreshold, bool doErode=true) {
      Mat mask;
      absdiff(f1,bg,mask);
      vector<Mat> channels;
      split(mask,channels);
      mask=channels[0];
      for (int i=1; i < channels.size(); i++) {
         mask=mask+channels[i];
      }
      threshold(mask,mask,thresh,255,THRESH_BINARY);
      if (doErode && boundaryDetectionKernelSize>0) {
         erode(mask,mask,kernel);
      }
      if (contourDetectionFilterContour) {
         vector<vector<Point> > contours;
         findContours(mask,contours,RETR_EXTERNAL,CHAIN_APPROX_NONE);
         int maxA=0;
         int maxIdx=-1;
         for (int i=0; i < contours.size(); i++)
            if (contourArea(contours[i]) > maxA) {
               maxA=contourArea(contours[i]);
               maxIdx=i;
            }
         mask=Mat::zeros(Size(mask.cols,mask.rows),CV_8UC1);
         drawContours(mask,contours,maxIdx,Scalar(255),FILLED);
      }
      if (!bhull.empty())
         mask.copyTo(mask,bhull);
      return mask;
   }

   Mat getComputedBg() {
      if (bgChanged) {
         computedBg=frame_max_x.clone();
         Rect frame_min_x_rect=Rect(max(0,left_max_x-contourDetectionKernelSize),0,bg.cols-left_max_x,bg.rows);
         Mat tmp=computedBg(frame_min_x_rect);
         frame_min_x(frame_min_x_rect).copyTo(tmp);

         bgChanged=false;
      }
      return computedBg;
   }

   Point estimateJoint(Point j1,Point j2, double j1j2length,double j1j2ratio,double topSolution=false,int recursion=1) {
      if (recursion > 3)
         return (j1+j2)/2;

      double l1=j1j2length*1.0/(j1j2ratio+1);
      double l2=j1j2length-l1;
      double a=(j1.x-j2.x);
      double b=j1.y-j2.y;

      if (a==0)
         return j1;
      double c=l2*l2-l1*l1+j1.x*j1.x-j2.x*j2.x+j1.y*j1.y-j2.y*j2.y;
      double d=c/2/a;
      double e=j2.x-d;

      // solving th quadratic equation
      double as=1+b*b/(a*a);
      double bs=2*(e*b/a-j2.y);
      double cs=e*e+j2.y*j2.y-l2*l2;
      if (as==0) {
         Point r;
         double y=-cs/bs;
         r.x=(int) d-y*b/a;
         r.y=(int) y;
         return r;
      } else {
         double D=bs*bs-4*as*cs;
         if (D < 0)
            return estimateJoint(j1,j2,j1j2length*1.1,j1j2ratio,topSolution,recursion+1);
         D=std::sqrt(D);
         D=topSolution? -bs-D : -bs+D;
         D=D/2/as;
         Point r;
         r.x=(int) d-D*b/a;
         r.y=(int) D;
         return r;
      }
   }
public:
   void registerNewObservation(Mat originalFrame) {
      timer.start("BGS_OWN_PHASE2_1");
      Mat frame;
      if (bbox.x==-1)
         frame=originalFrame;
      else
         frame=originalFrame(bbox);

      t=t+1;
      Mat pureMask=calcMask(frame,bg,contourDetectionThreshold,true);
      Mat mask;
      if (boundaryDetectionKernelSize>0) {
         erode(pureMask,mask,kernel);
      } else
         mask=pureMask.clone();
      Moments mom=moments(mask);
      if (mom.m00>0) {
         Rect currRect=boundingRect(mask);
         int right_curr_x=currRect.x+currRect.width;
         if (right_curr_x > right_max_x) {
            right_max_x=right_curr_x;
            left_max_x=currRect.x;
            frame_max_x=frame;
            mask_max_x=mask;
            bgChanged=true;

            if (should_make_flip_decision && t < contourDetectionLastFlipDecisionTime) {
               Mat top=mask(Rect(0,0,mask.cols,(int) mask.rows*contourDetectionFlipDecisionRatio));
               Moments mhead=moments(top);
               if (mhead.m00>0)
                  head_CM_max_x_estimate=mhead.m10/mhead.m00;
            }

            Mat legPart=mask(Rect(0,mask.rows-7,mask.cols,7));
            Rect ankleRect=boundingRect(legPart);
            anklePoint=Point(ankleRect.x+ankleRect.width,mask.rows-1);
         }
         double CM_curr_x=mom.m10/mom.m00;
         if (CM_curr_x < CM_min_x) {
            CM_min_x=CM_curr_x;
            frame_min_x=frame;
            bgChanged=true;

            if (should_make_flip_decision && t < contourDetectionLastFlipDecisionTime) {
               Mat top=mask(Rect(0,0,mask.cols,(int) mask.rows*contourDetectionFlipDecisionRatio));
               Moments mhead=moments(top);
               if (mhead.m00>0)
                  head_CM_min_x_estimate=mhead.m10/mhead.m00;
            }
         }
         double filtered_CM_curr_x=filtered_CM_last_x*(1-strokePhaseHysteresis)+CM_curr_x*strokePhaseHysteresis;
         timer.stop("BGS_OWN_PHASE2_1");
         timer.start("BGS_OWN_PHASE2_2");
         // track head
         Rect bbox2=boundingRect(mask);

         int headMinY=(int) bbox2.height*1.0/(1.0/componentTrackingHeadTorsoRatio+1.0)*2/3;
         Mat headPart=mask(Rect(bbox2.x,bbox2.y,bbox2.width,headMinY));
         distanceTransform(headPart,headPart,DIST_L2,3);
         double min_val,max_val;
         Point min_loc,max_loc;
         minMaxLoc(headPart,&min_val,&max_val,&min_loc,&max_loc);
         Point currHeadPosition=max_loc+bbox2.tl();
         int direction_curr=currHeadPosition.x-lastHeadPosition.x;
         int oldDirectionAvg=direction_avg;
         direction_avg=direction_curr*strokePhaseHysteresis+direction_avg*(1-strokePhaseHysteresis);
         lastHeadPosition=currHeadPosition;
         // compute stroke phases
         if (direction_avg < 0 && oldDirectionAvg >= 0 && t_catch < t_finish && t-videoFps*minPhaseTimeInSec > t_finish) {
            d_catch_catch=t-1-t_catch;
            t_catch=t-1;
         }
         else if (direction_avg > 0 && oldDirectionAvg <= 0 && t_finish <= t_catch && t-videoFps*minPhaseTimeInSec > t_catch) {
            d_finish_finish=t-1-t_finish;
            t_finish=t-1;
         }
         circle(frame,currHeadPosition,3,(0,0,255));
         #ifdef DEBUG
         headPart=headPart*16;
         cvtColor(headPart,headPart,COLOR_GRAY2BGR);
         circle(headPart,max_loc,3,(0,0,255));
         rectangle(headPart,lastHeadRect,Scalar(0,255,255));
         imshow("head",headPart);
         #endif


        if (contourDetectionEnableTracking) {
        // track hip
        if (CM_curr_x < CM_min_x*1.05 && right_curr_x > contourDetectionHipYDetectorWidth && t_finish>0 && t_catch > 0) {
               Rect thightBand=Rect(right_curr_x-contourDetectionHipYDetectorWidth,0,contourDetectionHipYDetectorWidth,mask.rows);
               Mat thight=mask(thightBand);
               Rect thightRect=boundingRect(thight);
               thightRect.x=thightRect.x+thightBand.x;
               thightRect.x=thightRect.x-contourDetectionKernelSize;
               thightRect.y=thightRect.y-contourDetectionKernelSize;
               thightRect.width=thightRect.width+2*contourDetectionKernelSize;
               thightRect.height=thightRect.height+2*contourDetectionKernelSize;
               //printf("thight:%i,%i,%i,%i\n",thightRect.x,thightRect.y,thightRect.width,thightRect.height);
               //rectangle(frame,thightRect,Scalar(255,255,0),2);
               if (hipy >=0 ) {
                  hipy=(int) ((thightRect.y+thightRect.height/2)*contourDetectionHipYAlpha+hipy*(1.0-contourDetectionHipYAlpha));
                  hip_topy=thightRect.y*contourDetectionHipYAlpha+hip_topy*(1.0-contourDetectionHipYAlpha);
                  hip_x_from_back=thightRect.height/2*contourDetectionHipYAlpha+hip_x_from_back*(1.0-contourDetectionHipYAlpha);
               } else {
                  hipy=thightRect.y+thightRect.height/2;
                  hip_topy=thightRect.y;
                  hip_x_from_back=thightRect.height/2;
               }
            }
         if (hipy >= 0) {
            Mat lowerBody=mask(Rect(0,hipy-2,frame.cols,4));
            hipx=boundingRect(lowerBody).x+hip_x_from_back;
            #ifdef DEBUG
            line(frame,Point(0,hipy),Point(frame.cols,hipy),Scalar(0,0,255));
            circle(frame,Point(hipx,hipy),3,Scalar(255,255,0),-1);
            #endif

            // track hand
            //erode(pureMask,mask,handKernel);
            mask=pureMask;
            bbox2=boundingRect(mask);
            headMinY=(int) bbox2.height*1.0/(1.0/componentTrackingHeadTorsoRatio+1.0)+bbox2.y;
            Mat handPart=mask(Rect(0,headMinY,mask.cols, max(0,hip_topy-headMinY-2)));
            if (handPart.cols > 0 && handPart.rows > 0) {
               Rect hand1=boundingRect(handPart);
               Mat handLoc=handPart(Rect(hand1.x+hand1.width-10,
                                    0,
                                    10,handPart.rows));
               Rect hand2=boundingRect(handLoc);
               Rect hand3=Rect(hand1.x+hand1.width-10+hand2.x,hand2.y+headMinY,hand2.width,hand2.height);
               Point newHandPoint=Point(hand3.x+hand3.width/2,hand3.y+min(hand3.height/2,3));
               handPoint=newHandPoint*0.9+0.1*handPoint;


               // and also track shoulder
                  // first estimate
               shoulderPoint.x=lastHeadPosition.x;
               shoulderPoint.y=(lastHeadPosition.y*3+hipy*1)/4;
                  // refining estimate
               Mat shoulder;
               distanceTransform(mask,shoulder,DIST_L2,3);
               shoulder=shoulder(Rect(max(0,shoulderPoint.x-contourDetectionShoulderPointSearchRectW/2),max(0,shoulderPoint.y-contourDetectionShoulderPointSearchRectH/2),min(contourDetectionShoulderPointSearchRectW,mask.cols),min(contourDetectionShoulderPointSearchRectH,mask.rows)));
               double min_,max_;
               Point minLoc,maxLoc;
               minMaxLoc(shoulder,&min_,&max_,&minLoc,&maxLoc);
               shoulderPoint=maxLoc+shoulderPoint+Point(-contourDetectionShoulderPointSearchRectW/2,-contourDetectionShoulderPointSearchRectH/2);

               // track elbow
               elbowPoint=(shoulderPoint+handPoint)/2;
               if (t_catch==t-1) {
                  shoulderLength=std::sqrt((shoulderPoint.x-handPoint.x)*(shoulderPoint.x-handPoint.x)
                                 +(shoulderPoint.y-handPoint.y)*(shoulderPoint.y-handPoint.y));
               } else {
                  elbowPoint=estimateJoint(handPoint,shoulderPoint,shoulderLength,contourDetectionUpperLowerArmRatio,false);
               }
           }
         }

         // track knee
         kneePoint=(Point_<int>) (Point(hipx,(int) hipy)+anklePoint)/2.0;
         if (t_finish==t-1) {
            thightLength=std::sqrt((kneePoint.x-anklePoint.x)*(kneePoint.x-anklePoint.x)
                                 +(kneePoint.y-anklePoint.y)*(kneePoint.y-anklePoint.y));
         } else {
            kneePoint=estimateJoint(anklePoint,Point(hipx,(int) hipy),thightLength*2,1.0,true);
         }

         // track back
         //  calc back curve 1
         backCurve.clear();
         for (int i=(int) hipy; i>=shoulderPoint.y; i=i-contourDetectionBackYStepSize) {
            Mat seg=mask(Rect(0,i,mask.cols,contourDetectionBackYStepSize));
            Rect r=boundingRect(seg);
            r.y=r.y+r.height/2+i;
            backCurve.push_back(Point(r.x,r.y));
         }

         //   calc back curve based on spine
         backIsHidedByElbow=false;
         vector<Point> spineCurve;
         Mat spineMat;
         distanceTransform(mask,spineMat,DIST_L2,3);
         for (int i=0; i<backCurve.size()*contourDetectionBackYStepSize; i=i+contourDetectionBackYStepSize) {
            int d=hipy-shoulderPoint.y;
            Point s;
            s.x=(shoulderPoint.x*i+hipx*(d-i))/d;
            s.y=(int) (shoulderPoint.y*i+hipy*(d-i))/d;

            while (spineMat.at<float>(s.y,s.x-1) >= spineMat.at<float>(s.y,s.x) && s.x>1)
               s.x--;
            while (spineMat.at<float>(s.y,s.x+1) >= spineMat.at<float>(s.y,s.x) && s.x<spineMat.cols-1)
               s.x++;
            spineCurve.push_back(s);
            if (s.y<=elbowPoint.y && s.y+contourDetectionBackYStepSize>elbowPoint.y && elbowPoint.x <= s.x)
               backIsHidedByElbow=true;
         }
         // merge back and spine curves
         if (backIsHidedByElbow && backCurve.size()> 0) {
            int offset=backCurve[0].x-spineCurve[0].x;
            for (int i=0; i<spineCurve.size(); i++) {
               backCurve[i].x=max(backCurve[i].x,spineCurve[i].x+offset);
            }
         }

         #ifdef DEBUG
         if (backCurve.size()> 0)
            for (int i=0; i<backCurve.size()-1;i++)
               line(frame,backCurve[i],backCurve[i+1],Scalar(255,0,120));


         circle(frame,kneePoint,3,Scalar(0,255,55),-1);
         circle(frame,handPoint,3,Scalar(0,0,255),-1);
         circle(frame,anklePoint,3,Scalar(128,0,255),-1);
         circle(frame,Point(hipx,(int) hipy),3,Scalar(128,128,255),-1);
         circle(frame,shoulderPoint,3,Scalar(0,255,55),-1);
         circle(frame,elbowPoint,4,Scalar(255,255,55),2);
         line(frame,Point(right_curr_x,0),Point(right_curr_x,frame.rows),Scalar(0,0,255));
         imshow("test",frame);
         /*distanceTransform(mask,mask,DIST_L2,3);
         double m,n;
         Point b,v;
         minMaxLoc(mask,&m,&n,&b,&v);*/
         imshow("test2",mask);
         #endif
         }

         direction_last=direction_curr;
         CM_last_x=CM_curr_x;
         filtered_CM_last_x=filtered_CM_curr_x;

         if (t-1==t_catch) {
            catchBackPos=shoulderPoint.x;
            catchHandPos=handPoint.x;
            catchHipPos=hipx;
         } else if (t-1==t_finish) {
            lastLegContribution=catchHipPos-hipx;
            lastBackContribution=catchBackPos-shoulderPoint.x-lastLegContribution;
            lastArmContribution=catchHandPos-handPoint.x-lastLegContribution-lastBackContribution;
         }

         timer.stop("BGS_OWN_PHASE2_2");
      }

      #ifdef DEBUG
      imshow("testBoundaryDetectionStatusMask",mask);
      imshow("testBoundaryDetectionStatus",frame);
      printf("testBoundaryDetectionStatus: minx: %f, maxx: %i, curr_x:%f, dir:%f\n",CM_min_x, right_max_x, CM_last_x,direction_last);
      #endif
   }

   int getProcessedFrameNumber() {
      return t;
   }

   bool isReady() {
      return t > contourDetectionWindowSize;
   }

   bool measurentsAreValid() {
      return contourDetectionEnableTracking && t_finish > 0 && t_catch > 0 && hipy >=0;
   }

   bool isDrivePhase() {
      return direction_last < 0;
   }

   bool isRecoveryPhase() {
      return !isDrivePhase();
   }

   /**
    Time of the last full stroke, in video frames
    */
   int lastStrokeTime() {
      return d_catch_catch;
   }

   /** in stroke-per-minute */
   double strokeRate() {
      return videoFps/lastStrokeTime()*60;
   }

   int getArmContribution() {
      return lastArmContribution;
   }

   int getBackContribution() {
      return lastBackContribution;
   }

   int getLegContribution() {
      return lastLegContribution;
   }

   int getTotalStrokeLength() {
      return lastLegContribution+lastArmContribution+lastBackContribution;
   }

   bool shouldFlip() {
      if (!should_make_flip_decision)
         return false;
      return head_CM_min_x_estimate > bg.cols-head_CM_max_x_estimate*contourDetectionFlipDecisionRatio2;
   }

   Point getHipPoint() {
      return Point((int) hipx+bbox.x,hipy+bbox.y);
   }

   int thightTopY() {
      return hip_topy+bbox.y;
   }

   Point getHandPoint() {
      return Point(handPoint.x+bbox.x,handPoint.y+bbox.y);
   }

   Point getAnklePoint() {
      return Point(anklePoint.x+bbox.x,anklePoint.y+bbox.y);
   }

   Point getKneePoint() {
      return kneePoint+bbox.tl();
   }

   Point getShoulderPoint() {
      return shoulderPoint+bbox.tl();
   }

   Point getElbowPoint() {
      return elbowPoint+bbox.tl();
   }

   Point getHeadPoint() {
      return lastHeadPosition+bbox.tl();
   }

   bool isBackHidedByElbow() {
      return backIsHidedByElbow;
   }

   vector<Point> getBackCurve() {
      vector<Point> r;
      for (int i=0; i<backCurve.size(); i++) {
         r.push_back(backCurve[i]+bbox.tl());
      }
      return r;
   }

   bool isCatch() {
      return t-1==t_catch;
   }

   bool isFinish() {
      return t-1==t_finish;
   }

   Mat getComputedMaskROI(Mat img) {
      Mat imgROI=img(bbox);
      Mat mask=calcMask(getComputedBg(),imgROI,contourDetectionThreshold2,false);
      if (!bhull.empty()) {
         Mat tmp=Mat::zeros(Size(mask.cols,mask.rows),CV_8UC1);
         mask.copyTo(tmp,bhull);
         mask=tmp;
      }
      return mask;
   }

   Mat getComputedMask(Mat img) {
      Mat mask=Mat::zeros(origSize, CV_8UC1);
      getComputedMaskROI(img).copyTo(mask(bbox));
      return mask;
   }

   ContourDetectionStatus(Mat bg, Rect bbox=Rect(-1,-1,-1,-1), Mat bhull=Mat(),bool shouldMakeFlipDecision=true) :
      bg(bg),
      right_max_x(-1),
      left_max_x(-1),
      CM_min_x(10000),
      CM_last_x(0),
      direction_last(0),
      bgChanged(true),
      t(0),
      bhull(bhull),
      bbox(bbox),
      origSize(Size(bg.cols,bg.rows)),
      t_catch(-1),
      t_finish(-1),
      d_catch_catch(-1),
      d_finish_finish(-1),
      filtered_CM_last_x(0),
      should_make_flip_decision(shouldMakeFlipDecision),
      head_CM_max_x_estimate(0),
      head_CM_min_x_estimate(0),
      hipy(-1.0),
      thight_top(0.0),
      hip_topy(-1),
      hip_x_from_back(-1),
      hipx(-1),
      handPoint(Point(0,0)),
      anklePoint(Point(0,0)),
      lastHeadPosition(Point(0,0)),
      direction_avg(0),
      shoulderPoint(0,0),
      elbowPoint(0,0)
   {
      if (this->bbox.x==-1)
         this->bbox=Rect(0,0,bg.cols,bg.rows);
      if (contourDetectionKernelSize>0)
         kernel = getStructuringElement( MORPH_RECT,
                                       Size( 2*contourDetectionKernelSize + 1, 2*contourDetectionKernelSize + 1 ),
                                       Point( contourDetectionKernelSize, contourDetectionKernelSize ) );
      if (contourDetectionHandKernelSize>0)
         handKernel = getStructuringElement( MORPH_RECT,
                                       Size( /*2*contourDetectionHandKernelSize +*/ 1, 2*contourDetectionHandKernelSize + 1 ),
                                       Point( 0/*contourDetectionHandKernelSize*/, contourDetectionHandKernelSize ) );

      if (!this->bhull.empty())
         this->bhull=this->bhull(this->bbox);
      this->bg=this->bg(this->bbox);
   }
};

class ComponentTracker {
private:
   Rect bbox;

   Point lastHeadPosition;
   Rect lastHeadRect;

   Mat erodeKernel;
   Mat dilateKernel;
public:
   void setNewBoundingBox(Rect b) {
      bbox=b;
   }

   void registerNewObservation(Mat imgROI) {
      Rect bbox2=boundingRect(imgROI);
      int headMinY=(int) bbox2.height*1.0/(1.0/componentTrackingHeadTorsoRatio+1.0)+bbox2.y;

      if (componentTrackingErodeKernelSize>0)
         erode(imgROI,imgROI,erodeKernel);
      if (componentTrackingDilateKernelSize>0)
         dilate(imgROI,imgROI,dilateKernel);
      Mat headPart=imgROI(Rect(0,0,imgROI.cols,headMinY));
      distanceTransform(headPart,headPart,DIST_L2,3);
      double min_val,max_val;
      Point min_loc,max_loc;
      minMaxLoc(headPart,&min_val,&max_val,&min_loc,&max_loc);
      Point currHeadPosition=max_loc+bbox.tl();
      lastHeadPosition=currHeadPosition;
      int headHeight=max_loc.y-headMinY*2;
      lastHeadRect=Rect(lastHeadPosition.x-headHeight/2,lastHeadPosition.y-headHeight/2,
                         headHeight,headHeight);

      #ifdef DEBUG
      headPart=headPart*16;
      cvtColor(headPart,headPart,COLOR_GRAY2BGR);
      circle(headPart,max_loc,3,(0,0,255));
      rectangle(headPart,lastHeadRect,Scalar(0,255,255));
      imshow("head",headPart);
      #endif
   }

   Point getLastHeadPosition() {
      return lastHeadPosition;
   }

   ComponentTracker()
   {
      if (componentTrackingErodeKernelSize>0)
         erodeKernel = getStructuringElement( MORPH_RECT,
                                       Size( 2*componentTrackingErodeKernelSize + 1, 2*componentTrackingErodeKernelSize + 1 ),
                                       Point( componentTrackingErodeKernelSize, componentTrackingErodeKernelSize ) );
      if (componentTrackingDilateKernelSize>0)
         dilateKernel = getStructuringElement( MORPH_RECT,
                                       Size( 2*componentTrackingDilateKernelSize + 1, 2*componentTrackingDilateKernelSize + 1 ),
                                       Point( componentTrackingDilateKernelSize, componentTrackingDilateKernelSize ) );
   }
};

enum DetectionPhase { BOUNDARY_DETECTION, CONTOUR_DETECTION, TRACKING };

class DetectorController {
   private:
      // store last frames
      queue<Mat> frameBuffer;
      int bufferSize;
      // status of detection
      BoundaryDetectionStatus* activeBoundaryDetectionStatus;
      BoundaryDetectionStatus* upcomingBoundaryDetectionStatus;
      ContourDetectionStatus* activeContourDetectionStatus;
      ContourDetectionStatus* upcomingContourDetectionStatus;
      ComponentTracker componentTracker;
      int t;
      DetectionPhase phase;
      bool shouldFlip;
      ofstream* dataF;

      Ptr<BackgroundSubtractorMOG2> bgs_gmm;
      Ptr<BackgroundSubtractorKNN> bgs_knn;

      void handlePhaseTransitions() {
         bool boundaryDetectionCanProceed=upcomingBoundaryDetectionStatus->isReady();
         bool contourDetectionCanProceed=(upcomingContourDetectionStatus==NULL) || (upcomingContourDetectionStatus->isReady());

         if (boundaryDetectionCanProceed && contourDetectionCanProceed) {
            if (phase==BOUNDARY_DETECTION)
               phase==CONTOUR_DETECTION;
            else if (phase==CONTOUR_DETECTION)
               phase==TRACKING;

            if (activeBoundaryDetectionStatus!=NULL)
               delete activeBoundaryDetectionStatus;
            activeBoundaryDetectionStatus=upcomingBoundaryDetectionStatus;
            upcomingBoundaryDetectionStatus=new BoundaryDetectionStatus();

            if (activeContourDetectionStatus!=NULL) {
               delete activeContourDetectionStatus;
            }
            if (upcomingContourDetectionStatus != NULL && upcomingContourDetectionStatus->shouldFlip()) {
               shouldFlip=true;
               delete upcomingContourDetectionStatus;
               upcomingContourDetectionStatus=NULL;
               phase=BOUNDARY_DETECTION;
            } else {
               activeContourDetectionStatus=upcomingContourDetectionStatus;
               upcomingContourDetectionStatus=new ContourDetectionStatus(
                     activeBoundaryDetectionStatus->getBg(),
                     activeBoundaryDetectionStatus->getDilatedBoundingBox(),
                     activeBoundaryDetectionStatus->getConvexHullMask());
               if (activeContourDetectionStatus != NULL) {
                  phase=TRACKING;
                  componentTracker.setNewBoundingBox(activeBoundaryDetectionStatus->getDilatedBoundingBox());
               }
            }
         }
      }

      void handleBoundaryDetectionStatus(Mat nextFrame) {
         upcomingBoundaryDetectionStatus->registerNewObservation(nextFrame);
      }

      void handleContourDetectionStatus(Mat nextFrame,Mat bg) {
         if (upcomingContourDetectionStatus!=NULL) {
            upcomingContourDetectionStatus->registerNewObservation(nextFrame);
         }
      }
   public:
      DetectorController(int bufferSize=3) :
         shouldFlip(false)
      {
         this->bufferSize=bufferSize;
         activeBoundaryDetectionStatus=NULL;
         upcomingBoundaryDetectionStatus=new BoundaryDetectionStatus();
         activeContourDetectionStatus=NULL;
         upcomingContourDetectionStatus=NULL;
         t=0;
         phase=BOUNDARY_DETECTION;

         dataF=new ofstream("data.csv",ios::out);

         bgs_gmm=createBackgroundSubtractorMOG2();
         bgs_knn=createBackgroundSubtractorKNN();

         bgs_gmm->setHistory(40);
         bgs_gmm->setNMixtures(1);
         bgs_gmm->setBackgroundRatio(0.2);
         bgs_gmm->setDetectShadows(false);

         bgs_knn->setHistory(40);
         bgs_knn->setNSamples(30);
         bgs_knn->setkNNSamples(3);
         //bgs_knn->setDist2Threshold(0.2);
         bgs_knn->setDetectShadows(false);
      }

      Timer getTimer() {
         return timer;
      }

      ~DetectorController() {
         delete upcomingBoundaryDetectionStatus;
         if (activeBoundaryDetectionStatus!=NULL)
            delete activeBoundaryDetectionStatus;
         if (upcomingContourDetectionStatus!=NULL)
            delete upcomingContourDetectionStatus;
        if (activeContourDetectionStatus!=NULL)
           delete activeContourDetectionStatus;

        delete dataF;
      }

      void loopDetector(Mat nextFrame) {
         t=t+1;

        timer.start("BGS_OWN");
         if (shouldFlip) {
            flip(nextFrame,nextFrame,1);
         }
         handlePhaseTransitions();
         timer.start("BGS_OWN_PHASE1");
         handleBoundaryDetectionStatus(nextFrame);
         timer.stop("BGS_OWN_PHASE1");
         if (activeBoundaryDetectionStatus!=NULL && !compareBackgroundSubtractionMethodsOnlyPhase1) {
            timer.start("BGS_OWN_PHASE2");
            #ifdef DEBUG
            Mat bg=activeBoundaryDetectionStatus->getBg();
            if (boundaryDetectionCalcBoundingBox)
               rectangle(bg,activeBoundaryDetectionStatus->getDilatedBoundingBox(),Scalar(0,0,255));
            if (boundaryDetectionCalcBoundingHull) {
                imshow("testBoundaryDetectionStatusConvexHull",activeBoundaryDetectionStatus->getConvexHullMask()*255);
            }
            imshow("testBoundaryDetectionStatusBG",bg);
            #endif
            handleContourDetectionStatus(nextFrame,activeBoundaryDetectionStatus->getBg());
            if (activeContourDetectionStatus!=NULL) {
              /* printf(upcomingContourDetectionStatus->isDrivePhase() ? "Drive\n" : "Recovery\n");
               double tstr=(upcomingContourDetectionStatus->lastStrokeTime()/30.0);
               double spm=((tstr==0) ? -1 : 60.0/tstr);
               printf("Last stroke time:%f,   %f SPM\n",tstr,spm);*/
               //printf(upcomingContourDetectionStatus->shouldFlip() ? "Flip!\n" : "No flip.\n");
               Mat mask=activeContourDetectionStatus->getComputedMaskROI(nextFrame);
               /*cvtColor(mask,mask,COLOR_GRAY2BGR);
               line(mask,Point(0,activeContourDetectionStatus->hipPosition()),Point(mask.cols,activeContourDetectionStatus->hipPosition()),Scalar(0,0,255));*/
               imshow("testContourDetectionStatusBG",mask);
               //componentTracker.registerNewObservation(mask);
            }
            if (upcomingContourDetectionStatus!=NULL && upcomingContourDetectionStatus->measurentsAreValid()) {
               Mat n=nextFrame.clone();
               Scalar red=Scalar(0,0,255);
               Scalar blue=Scalar(255,0,0);
               circle(n,upcomingContourDetectionStatus->getAnklePoint(),3,Scalar(200,200,255),-1);
               circle(n,upcomingContourDetectionStatus->getKneePoint(),3,red,-1);
               circle(n,upcomingContourDetectionStatus->getHipPoint(),3,Scalar(255,200,0),-1);
               circle(n,upcomingContourDetectionStatus->getHandPoint(),3,Scalar(255,0,0),-1);
               circle(n,upcomingContourDetectionStatus->getElbowPoint(),3,Scalar(0,255,0),-1);
               circle(n,upcomingContourDetectionStatus->getShoulderPoint(),3,Scalar(0,255,250),-1);
               circle(n,upcomingContourDetectionStatus->getHeadPoint(),3,Scalar(255,30,230),-1);
               vector<Point> backCurve=upcomingContourDetectionStatus->getBackCurve();
               if (backCurve.size() > 0)
                  for (int i=0; i < backCurve.size()-1; i++)
                     if (upcomingContourDetectionStatus->isBackHidedByElbow())
                        line(n,backCurve[i],backCurve[i+1],red);
                     else
                        line(n,backCurve[i],backCurve[i+1],blue);
               char s[500];
               sprintf((char*) &s,"Arm:%.1f%%, back:%.1f%%, leg:%.1f%%\n",
                     ((double) upcomingContourDetectionStatus->getArmContribution())/upcomingContourDetectionStatus->getTotalStrokeLength()*100,
                     ((double) upcomingContourDetectionStatus->getBackContribution())/upcomingContourDetectionStatus->getTotalStrokeLength()*100,
                     ((double) upcomingContourDetectionStatus->getLegContribution())/upcomingContourDetectionStatus->getTotalStrokeLength()*100);
               putText(n,s,Point(5,n.rows-2),FONT_HERSHEY_COMPLEX,0.36,Scalar(100,80,200));
               if (upcomingContourDetectionStatus->isFinish()) {
                  printf("Finish -- Arm:%.1f%%, back:%.1f%%, leg:%.1f%%\n",
                     ((double) upcomingContourDetectionStatus->getArmContribution())/upcomingContourDetectionStatus->getTotalStrokeLength()*100,
                     ((double) upcomingContourDetectionStatus->getBackContribution())/upcomingContourDetectionStatus->getTotalStrokeLength()*100,
                     ((double) upcomingContourDetectionStatus->getLegContribution())/upcomingContourDetectionStatus->getTotalStrokeLength()*100);
               } else if (upcomingContourDetectionStatus->isCatch())
                   printf("Catch.\n");
               imshow("tracking",n);

               *dataF << upcomingContourDetectionStatus->getAnklePoint().x;
               *dataF << ";" << upcomingContourDetectionStatus->getAnklePoint().y;
               *dataF << ";" << upcomingContourDetectionStatus->getKneePoint().x;
               *dataF << ";" << upcomingContourDetectionStatus->getKneePoint().y;
               *dataF << ";" << upcomingContourDetectionStatus->getHipPoint().x;
               *dataF << ";" << upcomingContourDetectionStatus->getHipPoint().y;
               *dataF << ";" << upcomingContourDetectionStatus->getHandPoint().x;
               *dataF << ";" << upcomingContourDetectionStatus->getHandPoint().y;
               *dataF << ";" << upcomingContourDetectionStatus->getElbowPoint().x;
               *dataF << ";" << upcomingContourDetectionStatus->getElbowPoint().y;
               *dataF << ";" << upcomingContourDetectionStatus->getShoulderPoint().x;
               *dataF << ";" << upcomingContourDetectionStatus->getShoulderPoint().y;
               *dataF << ";" << upcomingContourDetectionStatus->getHeadPoint().x;
               *dataF << ";" << upcomingContourDetectionStatus->getHeadPoint().y;
               *dataF << ";" << upcomingContourDetectionStatus->getBackCurve();
            }
            timer.stop("BGS_OWN_PHASE2");
         }
         timer.stop("BGS_OWN");



         Mat sumI=Mat::zeros(nextFrame.rows,nextFrame.cols*3,CV_8UC1);
         if (activeBoundaryDetectionStatus!=NULL && compareBackgroundSubtractionMethods) {
             Mat bgs_mask_own=activeBoundaryDetectionStatus->getBg();
             imshow("BGS_OWN_PHASE11",bgs_mask_own);
             timer.start("BGS_OWN_PHASE1");
             timer.start("BGS_OWN_PHASE1_ONLY_DIFF");
             absdiff(bgs_mask_own,nextFrame,bgs_mask_own);
             vector<Mat> channels;
             split(bgs_mask_own,channels);
             bgs_mask_own=channels[0];
             for (int i=1; i < channels.size(); i++) {
                bgs_mask_own=bgs_mask_own+channels[i];
             }
             threshold(bgs_mask_own,bgs_mask_own,60,1,THRESH_BINARY);
             timer.stop("BGS_OWN_PHASE1");
             timer.stop("BGS_OWN_PHASE1_ONLY_DIFF");
             Mat kernel = getStructuringElement( MORPH_RECT,
                                       Size( 3,3));
            erode(bgs_mask_own,bgs_mask_own,kernel);
            sumI(Rect(0,0,nextFrame.cols,nextFrame.rows))=bgs_mask_own*255;
            imshow("BGS_OWN_PHASE1",bgs_mask_own*255);
         }

         if (compareBackgroundSubtractionMethods) {
             Mat gmm_fg;
             timer.start("BGS_GMM");
             bgs_gmm.get()->apply(nextFrame,gmm_fg);
             timer.stop("BGS_GMM");
             Mat knn_fg;
             timer.start("BGS_KNN");
             bgs_knn.get()->apply(nextFrame,knn_fg);
             timer.stop("BGS_KNN");

             gmm_fg.convertTo(gmm_fg,CV_8UC1);
             imshow("BGS_GMM",gmm_fg);
             imshow("BGS_KNN",knn_fg);
             gmm_fg.copyTo(sumI(Rect(nextFrame.cols,0,nextFrame.cols,nextFrame.rows)));
             knn_fg.copyTo(sumI(Rect(2*nextFrame.cols,0,nextFrame.cols,nextFrame.rows)));
         }
         //imshow("comparison",sumI);
      }
};

#endif



